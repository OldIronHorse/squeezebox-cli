import pytest

import squeezebox_cli.tui.app


@pytest.fixture
def playlist():
    return [
            {
                'id': 52734,
                'title': 'City of Love Long',
                'album': 'City of Love',
                'artist': 'Deacon Blue',
                },
            {
                'id': 52807,
                'title': 'the 1',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52816,
                'title': 'cardigan',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52818,
                'title': 'exile',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52819,
                'title': 'hoax',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            ]


@pytest.fixture
def playlist_long_album():
    return [
            {
                'id': 52734,
                'title': 'City of Love',
                'album': 'City of Love Long',
                'artist': 'Deacon Blue',
                },
            {
                'id': 52807,
                'title': 'the 1',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52816,
                'title': 'cardigan',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52818,
                'title': 'exile',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52819,
                'title': 'hoax',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            ]


@pytest.fixture
def playlist_long_artist():
    return [
            {
                'id': 52734,
                'title': 'City of Love',
                'album': 'City of Love',
                'artist': 'Deacon Blue Long',
                },
            {
                'id': 52807,
                'title': 'the 1',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52816,
                'title': 'cardigan',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52818,
                'title': 'exile',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            {
                'id': 52819,
                'title': 'hoax',
                'album': 'folklore',
                'artist': 'Taylor Swift',
                },
            ]


def test_format_playlist_no_truncate(playlist):
    assert (2, 21, 35,
            [
                ('City of Love Long', 'City of Love', 'Deacon Blue'),
                ('the 1', 'folklore', 'Taylor Swift'),
                ('cardigan', 'folklore', 'Taylor Swift'),
                ('exile', 'folklore', 'Taylor Swift'),
                ('hoax', 'folklore', 'Taylor Swift'),
                ]) == squeezebox_cli.tui.app.format_playlist(playlist, 100)


def test_format_playlist_truncate_title(mocker, playlist):
    assert (2, 19, 33,
            [
                ('City of Love L…', 'City of Love', 'Deacon Blue'),
                ('the 1', 'folklore', 'Taylor Swift'),
                ('cardigan', 'folklore', 'Taylor Swift'),
                ('exile', 'folklore', 'Taylor Swift'),
                ('hoax', 'folklore', 'Taylor Swift'),
                ]) == squeezebox_cli.tui.app.format_playlist(playlist, 45)


def test_format_playlist_truncate_album(mocker, playlist_long_album):
    assert (2, 16, 33,
            [
                ('City of Love', 'City of Love L…', 'Deacon Blue'),
                ('the 1', 'folklore', 'Taylor Swift'),
                ('cardigan', 'folklore', 'Taylor Swift'),
                ('exile', 'folklore', 'Taylor Swift'),
                ('hoax', 'folklore', 'Taylor Swift'),
                ]) == squeezebox_cli.tui.app.format_playlist(
                        playlist_long_album, 45)


def test_format_playlist_truncate_artist(mocker, playlist_long_artist):
    assert (2, 16, 30,
            [
                ('City of Love', 'City of Love', 'Deacon Blue Lo…'),
                ('the 1', 'folklore', 'Taylor Swift'),
                ('cardigan', 'folklore', 'Taylor Swift'),
                ('exile', 'folklore', 'Taylor Swift'),
                ('hoax', 'folklore', 'Taylor Swift'),
                ]) == squeezebox_cli.tui.app.format_playlist(
                        playlist_long_artist, 45)


def test_trim_to_length_shorter():
    assert 'Taylor Swift' == squeezebox_cli.tui.app.trim_to(
            15, 'Taylor Swift')


def test_trim_to_length_equal():
    assert 'Taylor Swift' == squeezebox_cli.tui.app.trim_to(
            12, 'Taylor Swift')


def test_trim_to_length_just_shorter():
    assert 'Taylor Swi…' == squeezebox_cli.tui.app.trim_to(
            11, 'Taylor Swift')


def test_trim_to_length_much_shorter():
    assert 'Tay…' == squeezebox_cli.tui.app.trim_to(
            4, 'Taylor Swift')
