#!/bin/bash


echo "flake8..."
flake8 setup.py squeezebox_cli bin/* --per-file-ignores="__init__.py:F401,squeezebox:F811"
echo "bandit..."
bandit -r squeezebox_cli bin/*
echo "coverage..."
coverage run --source=squeezebox_cli --omit="/*tests*" -m nose
coverage html
coverage report
echo "done."
