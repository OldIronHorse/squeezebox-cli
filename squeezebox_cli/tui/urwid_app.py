import urwid
import functools
from enum import Enum
from tqdm import tqdm
import squeezebox_cli.player
from squeezebox_cli.player import Shuffle, Repeat
from squeezebox_cli.display .status import shuffle_display_string, \
        repeat_display_string

palette = [
        ('in focus', 'standout', ''),
        ('summary', 'bold,yellow', ''),
        ('playing', 'bold,yellow', ''),
        ('playlist', '', ''),
        ('heading', 'bold', ''),
        ]


State = Enum('State', 'CHOOSE_PLAYER SHOW SEARCH BROWSE EXIT')
state = State.CHOOSE_PLAYER


def urwid_main(server):
    while state != State.EXIT:
        match state:
            case State.CHOOSE_PLAYER:
                player = choose_player(server)
            case State.SHOW:
                show_status(server, player)
            case State.SEARCH:
                search(server, player)
            case State.BROWSE:
                browse(server, player)


def browse(server, player):
    global state
    urwid.MainLoop(BrowseFrame(server, player), palette=palette).run()
    state = State.SHOW


class BrowseFrame(urwid.Frame):
    def __init__(self, server, player):
        self.breadcrumbs = urwid.Columns([])
        self.path = [
                [
                    ('Artists',
                        lambda: squeezebox_cli.database.artists(server)),
                    ('Albums', lambda: squeezebox_cli.database.albums(server)),
                    ('Genres', lambda: squeezebox_cli.database.genres(server)),
                    ],
                ]
        self.browse_list = urwid.ListBox(urwid.SimpleFocusListWalker([]))
        self.browse_list.body = [urwid.SelectableIcon(name) for name, _func
                                 in self.path[-1]]
        super().__init__(self.browse_list, header=self.breadcrumbs)

    def keypress(self, size, key):
        global state
        match key:
            case 'esc':
                if not self.breadcrumbs.contents:
                    state = State.SHOW
                    raise urwid.ExitMainLoop()
                self.breadcrumbs.content = self.breadcrumbs.contents[:-1]
            case _:
                super().keypress(size, key)


def search(server, player):
    global state
    urwid.MainLoop(SearchFrame(server, player), palette=palette).run()
    state = State.SHOW


class SearchFrame(urwid.Frame):
    def __init__(self, server, player):
        self.searchterm = urwid.Edit(caption='/')
        super().__init__(
                SearchColumns(server, player),
                header=self.searchterm,
                focus_part='header')

    def keypress(self, size, key):
        match key:
            case 'enter' if self.focus_position == 'header':
                self.body.search(self.searchterm.edit_text)
                self.focus_position = 'body'
            case 'esc' if self.focus_position == 'header':
                raise urwid.ExitMainLoop()
            case 'esc' if self.focus_position == 'body':
                self.focus_position = 'header'
            case _:
                return super().keypress(size, key)


class SearchColumns(urwid.Columns):
    def __init__(self, server, player):
        self.server = server
        self.player = player
        self.artist_results = urwid.ListBox(urwid.SimpleFocusListWalker([]))
        self.album_results = urwid.ListBox(urwid.SimpleFocusListWalker([]))
        self.track_results = urwid.ListBox(urwid.SimpleFocusListWalker([]))
        super().__init__(
                [
                    urwid.Frame(self.artist_results,
                                header=urwid.Text(('heading', 'Artists:'))),
                    urwid.Frame(self.album_results,
                                header=urwid.Text(('heading', 'Albums:'))),
                    urwid.Frame(self.track_results,
                                header=urwid.Text(('heading', 'Tracks:'))),
                    ],
                dividechars=2)

    def search(self, term):
        self.track_results.body.append(urwid.Text('Searching...'))
        self.album_results.body.append(urwid.Text('Searching...'))
        self.artist_results.body.append(urwid.Text('Searching...'))
        self.results = squeezebox_cli.database.search(self.server, term)
        self.artist_results.body = [urwid.SelectableIcon(name) for _, name
                                    in self.results.get('artists', {}).items()]
        self.album_results.body = [urwid.SelectableIcon(name) for _, name
                                   in self.results.get('albums', {}).items()]
        self.track_results.body = [urwid.SelectableIcon(name) for _, name
                                   in self.results.get('tracks', {}).items()]
        self.artist_ids = [id for id, _
                           in self.results.get('artists', {}).items()]
        self.album_ids = [id for id, _
                          in self.results.get('albums', {}).items()]
        self.track_ids = [id for id, _
                          in self.results.get('tracks', {}).items()]

    def keypress(self, size, key):
        match key:
            case 'tab':
                focus_col = self.focus_position + 1
                if focus_col >= len(self.contents):
                    focus_col = 0
                self.focus_position = focus_col
            case 'enter':
                match self.focus_position:
                    case 0:
                        id_dict = {'artist_id': self.artist_ids[
                            self.artist_results.focus_position]}
                    case 1:
                        id_dict = {'album_id': self.album_ids[
                            self.album_results.focus_position]}
                    case 2:
                        id_dict = {'track_id': self.track_ids[
                            self.track_results.focus_position]}
                squeezebox_cli.player.play(self.server, self.player, **id_dict)
                raise urwid.ExitMainLoop()
            case 'a':
                match self.focus_position:
                    case 0:
                        id_dict = {'artist_id': self.artist_ids[
                            self.artist_results.focus_position]}
                    case 1:
                        id_dict = {'album_id': self.album_ids[
                            self.album_results.focus_position]}
                    case 2:
                        id_dict = {'track_id': self.track_ids[
                            self.track_results.focus_position]}
                squeezebox_cli.player.playlist_add(
                        self.server, self.player, **id_dict)
                raise urwid.ExitMainLoop()
            case 'i':
                match self.focus_position:
                    case 0:
                        id_dict = {'artist_id': self.artist_ids[
                            self.artist_results.focus_position]}
                    case 1:
                        id_dict = {'album_id': self.album_ids[
                            self.album_results.focus_position]}
                    case 2:
                        id_dict = {'track_id': self.track_ids[
                            self.track_results.focus_position]}
                squeezebox_cli.player.playlist_insert(
                        self.server, self.player, **id_dict)
                raise urwid.ExitMainLoop()
            case _:
                return super().keypress(size, key)


def show_status(server, player):
    print('Warming the cache...')
    playlist = squeezebox_cli.player.status(server, player)['playlist']
    for id, _title in tqdm(playlist):
        songinfo(server, id)
    displaylist = PlaylistBox(server, player)
    loop = urwid.MainLoop(urwid.Frame(displaylist, header=displaylist.summary),
                          palette=palette)
    displaylist.query_status(loop, None)
    loop.run()


class PlaylistBox(urwid.ListBox):
    def __init__(self, server, player):
        super().__init__(urwid.SimpleFocusListWalker([]))
        self.server = server
        self.player = player
        self.player_name = urwid.Text(('summary', 'player...'))
        self.mode = urwid.Text(('summary', 'mode...'))
        self.track_of = urwid.Text(('summary', 'track of...'))
        self.volume = urwid.Text(('summary', 'volume...'))
        self.repeat = urwid.Text(('summary', 'repeat...'))
        self.shuffle = urwid.Text(('summary', 'shuffle...'))
        self.summary = urwid.Columns(
                [self.player_name, self.mode, self.track_of, self.volume,
                 self.repeat, self.shuffle],
                dividechars=1)
        self.editing = False

    def query_status(self, loop, _):
        status = squeezebox_cli.player.status(self.server, self.player)
        self.player_name.set_text(('summary', status['name']))
        self.mode.set_text(('summary', f'm:{status["mode"]}'))
        t = ('t:-/-' if status['playlist_cur_index'] is None else
             f't:{status["playlist_cur_index"] + 1}'
             f'/{len(status["playlist"])}')
        self.track_of.set_text(('summary', t))
        self.volume.set_text(('summary', f'v:{status["volume"]}/100'))
        self.shuffle.set_text(
            ('summary',
             's:'
             f"{shuffle_display_string[status.get('shuffle', Shuffle.NONE)]}"))
        self.repeat.set_text(
            ('summary',
             'r:'
             f"{repeat_display_string[status.get('repeat', Repeat.NONE)]}"))
        if not self.editing:
            self.body.clear()
            for i, (id, title) in enumerate(status['playlist']):
                info = songinfo(self.server, id)
                format = 'playlist'
                focus_map = 'in focus'
                if i == status['playlist_cur_index']:
                    format = 'playing'
                    focus_map = None
                self.body.append(urwid.AttrMap(
                    urwid.Columns(
                        [
                            (4,
                             urwid.SelectableIcon((format, f'{i + 1}:'))),
                            ('weight', 4, urwid.Text((format, title),
                                                     wrap='ellipsis')),
                            ('weight', 4,
                             urwid.Text((format, info.get('album', '')),
                                        wrap='ellipsis')),
                            ('weight', 2,
                             urwid.Text((format, info.get('artist', '')),
                                        wrap='ellipsis')),
                            ],
                        dividechars=2),
                    None, focus_map=focus_map))
            if status['playlist_cur_index'] is not None:
                self.set_focus(status['playlist_cur_index'])
        loop.set_alarm_in(0.5, self.query_status)

    def keypress(self, size, key):
        global state
        match key:
            case ' ':
                squeezebox_cli.player.pause(self.server, self.player)
            case 'h':
                squeezebox_cli.player.previous(self.server, self.player)
            case 'l':
                squeezebox_cli.player.next(self.server, self.player)
            case '+':
                squeezebox_cli.player.change_volume(
                    self.server, self.player, 5)
            case '-':
                squeezebox_cli.player.change_volume(
                    self.server, self.player, -5)
            case 'm':
                squeezebox_cli.player.toggle_mute(self.server, self.player)
            case 's':
                squeezebox_cli.player.playlist_toggle_shuffle(
                    self.server, self.player)
            case 'r':
                squeezebox_cli.player.playlist_toggle_repeat(
                    self.server, self.player)
            case 'esc' if not self.editing:
                state = State.EXIT
                raise urwid.ExitMainLoop()
            case '/':
                state = State.SEARCH
                raise urwid.ExitMainLoop()
            case 'b':
                state = State.BROWSE
                raise urwid.ExitMainLoop()
            case 'up':
                self.editing = True
            case 'down':
                self.editing = True
        if self.editing:
            match key:
                case 'esc':
                    self.editing = False
                case 'enter':
                    squeezebox_cli.player.play(self.server,
                                               self.player,
                                               index=self.focus_position)
                    self.editing = False
                case 'd':
                    squeezebox_cli.player.playlist_remove(
                            self.server, self.player, self.focus_position)
                    self.editing = False
        super().keypress(size, key)


def choose_player(server):
    players = squeezebox_cli.player.players(server)
    player = None

    def on_click(button, choice):
        nonlocal player
        player = choice
        raise urwid.ExitMainLoop()

    main = urwid.Padding(
            menu('Choose player:', [p['name'] for p in players], on_click),
            left=2,
            right=2)
    urwid.MainLoop(main, palette=palette).run()
    global state
    state = State.SHOW
    return player


def menu(title, choices, handler):
    body = [urwid.Text(title), urwid.Divider()]
    for c in choices:
        button = urwid.Button(c)
        urwid.connect_signal(button, 'click', handler, c)
        body.append(urwid.AttrMap(button, None, focus_map='in focus'))
    return urwid.ListBox(urwid.SimpleFocusListWalker(body))


@functools.cache
def songinfo(server, id):
    return squeezebox_cli.database.songinfo(server, id)
