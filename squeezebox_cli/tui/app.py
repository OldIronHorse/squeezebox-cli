from dataclasses import dataclass
import curses
import functools
from enum import Enum
from curses_toolkit import choose, ColumnList, Scrollbar, Header, actions, \
        BlockingInput, NonblockingInput, Echo, Cursor, List, Text
from curses_toolkit.actions import keys_vim_up_down

import squeezebox_cli.player
import squeezebox_cli.database
from squeezebox_cli.display .status import shuffle_display_string, \
        repeat_display_string

State = Enum('State', {
    'CHOOSE_PLAYER': 0,
    'SHOW': 1,
    'SEARCH': 2,
    'EXIT': 3,
    })


Type = Enum('Type', 'ARTIST ALBUM TRACK')
type_strings = {
        Type.ARTIST: 'Artists:',
        Type.ALBUM: 'Albums:',
        Type.TRACK: 'Tracks:',
        }


@dataclass
class Context:
    state: State
    server: (str, int)
    player: str


@dataclass
class StatusContext(Context):
    status: dict


@dataclass
class SearchContext(Context):
    type: Type
    results_by_type: dict

    def results(self):
        return self.results_by_type[self.type]


@functools.cache
def songinfo(server, id):
    return squeezebox_cli.database.songinfo(server, id)


def main(server):
    def run_tui(screen):
        curses.curs_set(0)
        screen.keypad(True)
        state = State.CHOOSE_PLAYER
        player = None
        while state != State.EXIT:
            if state == State.CHOOSE_PLAYER:
                state, player = choose_player(server)
            if state == State.SHOW:
                state = show(server, player, state)
            if state == State.SEARCH:
                state = search(server, player, state)
            screen.refresh()
    curses.wrapper(run_tui)


def play(server, player, type, id):
    match type:
        case Type.ARTIST:
            squeezebox_cli.player.play(server, player, artist_id=id)
        case Type.ALBUM:
            squeezebox_cli.player.play(server, player, album_id=id)
        case Type.TRACK:
            squeezebox_cli.player.play(server, player, track_id=id)


def add(server, player, type, id):
    match type:
        case Type.ARTIST:
            squeezebox_cli.player.playlist_add(server, player, artist_id=id)
        case Type.ALBUM:
            squeezebox_cli.player.playlist_add(server, player, album_id=id)
        case Type.TRACK:
            squeezebox_cli.player.playlist_add(server, player, track_id=id)


def insert(server, player, type, id):
    match type:
        case Type.ARTIST:
            squeezebox_cli.player.playlist_insert(server, player, artist_id=id)
        case Type.ALBUM:
            squeezebox_cli.player.playlist_insert(server, player, album_id=id)
        case Type.TRACK:
            squeezebox_cli.player.playlist_insert(server, player, track_id=id)


def search(server, player, state):
    search_term_win = curses.newwin(1, curses.COLS, 0, 0)
    search_term_win.keypad(True)
    search_term_win.addstr(0, 0, '/')
    search_term_win.refresh()
    with Echo():
        with Cursor():
            term = search_term_win.getstr(0, 1, curses.COLS - 2)
    results = squeezebox_cli.database.search(server, term)
    results_list = List(2, 0,
                        curses.LINES - 2, curses.COLS)
    type_text = Text(1, 0, 1, curses.COLS,
                     attrs=curses.A_BOLD | curses.A_UNDERLINE)
    results_by_type = {
            Type.ARTIST: [(name, id)
                          for id, name in results['artists'].items()],
            Type.ALBUM: [(name, id) for id, name in results['albums'].items()],
            Type.TRACK: [(name, id) for id, name in results['tracks'].items()],
            }
    context = SearchContext(
            state, server, player, Type.ARTIST, results_by_type)
    with BlockingInput():
        while context.state == State.SEARCH:
            type_text.set_text(type_strings[context.type])
            type_text.show()
            results_list.clear()
            for name, id in results_by_type[context.type]:
                results_list.add_row(name)
            results_list.set_focus(0)
            results_list.show()
            prev_type = context.type
            while (context.type == prev_type
                   and context.state == State.SEARCH):
                actions(results_list,
                        context,
                        [keys_vim_up_down, keys_search],
                        initial_focus=0)
    return context.state


def keys_search(results_list, ctx, key):
    match key:
        case '\t':
            if ctx.type is Type.ARTIST:
                ctx.type = Type.ALBUM
            elif ctx.type is Type.ALBUM:
                ctx.type = Type.TRACK
            else:
                ctx.type = Type.ARTIST
            return True
        case 'j':
            results_list.set_focus(results_list.focus + 1)
            return False
        case 'k':
            results_list.set_focus(results_list.focus - 1)
            return False
        case 'p':
            play(ctx.server, ctx.player, ctx.type,
                 ctx.results()[results_list.focus][1])
            ctx.state = State.SHOW
            return True
        case 'a':
            add(ctx.server, ctx.player, ctx.type,
                ctx.results()[results_list.focus][1])
            ctx.state = State.SHOW
            return True
        case 'i':
            insert(ctx.server, ctx.player, ctx.type,
                   ctx.results()[results_list.focus][1])
            ctx.state = State.SHOW
            return True
        case 'x':
            ctx.state = State.SHOW
            return True


def choose_player(server):
    return State.SHOW, choose([p['name']
                               for p in squeezebox_cli.player.players(server)],
                              title='Choose player:')


def show(server, player, state):
    status_win = curses.newwin(1, curses.COLS, 0, 0)
    playlist_list = ColumnList(2, 0,
                               curses.LINES - 2, curses.COLS - 2,
                               column_count=3)
    Scrollbar(playlist_list)
    Header(playlist_list,
           ('', 'Track:', 'Album:', 'Artist:'),
           attrs=curses.A_BOLD | curses.A_UNDERLINE)
    curses.halfdelay(5)
    while state == State.SHOW:
        status = squeezebox_cli.player.status(server, player)
        playlist = status['playlist']
        summary = (f"{status['name']}:"
                   f" <{status['mode']}>"
                   f" {status['playlist_cur_index'] + 1}"
                   f"/{len(playlist)}"
                   f" vol:{status['volume']}/100")
        try:
            summary += f" s:{shuffle_display_string[status['shuffle']]}"
        except KeyError:
            pass
        try:
            summary += f" r:{repeat_display_string[status['repeat']]}"
        except KeyError:
            pass
        status_win.clear()
        status_win.addstr(0, 0, trim_to(curses.COLS, summary))
        status_win.refresh()
        title_col, album_col, artist_col, display_playlist = (
                format_playlist([songinfo(server, id)
                                 if id > 0 else {'title': title}
                                 for id, title in playlist],
                                curses.COLS))
        playlist_list.clear()
        for i, track in enumerate(display_playlist):
            title, album, artist = track
            playing = '>' if i == status['playlist_cur_index'] else ' '
            playlist_list.add_row((playing, title, album, artist))
        playlist_list.show(status['playlist_cur_index'])
        try:
            context = {}
            with NonblockingInput(5):
                context = actions(playlist_list,
                                  StatusContext(state, server, player, status),
                                  [keys_playlist])
            state = context.state
        except curses.error:
            # TODO: check type?
            pass
        # TODO: state in context
    playlist_list.clear()
    playlist_list.show()
    status_win.clear()
    status_win.refresh()
    return state


def choose_play_or_delete(playlist_list, context):

    def keys_delete_play(ct_list, context, key):
        match key:
            case 'd':
                squeezebox_cli.player.playlist_remove(context.server,
                                                      context.player,
                                                      ct_list.focus)
                return True
            case '\n':
                squeezebox_cli.player.play(context.server,
                                           context.player,
                                           index=ct_list.focus)
                return True
            case 'x':
                return True

    with BlockingInput():
        actions(playlist_list,
                context,
                [keys_vim_up_down, keys_delete_play],
                initial_focus=context.status['playlist_cur_index'])


def keys_playlist(ct_list, context, key):
    match key:
        case 'x':
            context.state = State.EXIT
        case '/':
            context.state = State.SEARCH
        case 'p':
            context.state = State.CHOOSE_PLAYER
        case 'h':
            squeezebox_cli.player.previous(context.server, context.player)
        case 'l':
            squeezebox_cli.player.next(context.server, context.player)
        case ' ':
            squeezebox_cli.player.pause(context.server, context.player)
        case '+':
            squeezebox_cli.player.change_volume(context.server,
                                                context.player, 5)
        case '-':
            squeezebox_cli.player.change_volume(context.server,
                                                context.player, -5)
        case 'm':
            squeezebox_cli.player.toggle_mute(context.server, context.player)
        case 's':
            squeezebox_cli.player.playlist_toggle_shuffle(context.server,
                                                          context.player)
        case 'r':
            squeezebox_cli.player.playlist_toggle_repeat(context.server,
                                                         context.player)
        case 'j':
            choose_play_or_delete(ct_list, context)
        case 'k':
            choose_play_or_delete(ct_list, context)
    return True


def format_playlist(playlist, width):
    return do_format_playlist(
            [(song.get('title', ' ' * 6),
              song.get('album', ' ' * 6),
              song.get('artist', ' ' * 7))
                for song in playlist],
            width)


def do_format_playlist(playlist, width):
    title_max_len = max([len(title)
                         for title, album, artist
                         in playlist])
    album_max_len = max([len(album)
                         for title, album, artist
                         in playlist])
    artist_max_len = max([len(artist)
                          for title, album, artist
                          in playlist])
    title_col = 2
    album_col = title_col + title_max_len + 2
    artist_col = album_col + album_max_len + 2
    overrun = artist_col + artist_max_len - width
    if overrun > 0:
        if title_max_len > album_max_len and title_max_len > artist_max_len:
            return do_format_playlist(
                [(trim_to(title_max_len - overrun, title), album, artist)
                 for title, album, artist in playlist],
                width)
        if album_max_len > artist_max_len:
            return do_format_playlist(
                [(title, trim_to(album_max_len - overrun, album), artist)
                 for title, album, artist in playlist],
                width)
        return do_format_playlist(
                    [(title, album, trim_to(artist_max_len - overrun, artist))
                        for title, album, artist in playlist],
                    width)

    return title_col, album_col, artist_col, playlist


def trim_to(length, s):
    if len(s) > length:
        return s[:length - 1] + '…'
    return s
